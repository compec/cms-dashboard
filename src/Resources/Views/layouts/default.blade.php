<!DOCTYPE html>
<html lang="<?php echo config('application.language') ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<title>{{ config('administrator.title') }}</title>
	<link media="all" type="text/css" rel="stylesheet" href="{{ asset('packages/frozennode/administrator/js/jquery/customscroll/customscroll.css') }}">
	<link media="all" type="text/css" rel="stylesheet" href="{{ asset('packages/frozennode/administrator/css/main.css') }}">
	<link media="all" type="text/css" rel="stylesheet" href="{{ asset('packages/mambo/cms/css/dashboard.css') }}">
	<link media="all" type="text/css" rel="stylesheet" href="{{ asset('packages/mambo/cms/css/frozennode_administrator_fix.css') }}">
    <link media="all" type="text/css" rel="stylesheet" href="{{ asset('packages/mambo/cms/js/jquery-loadmask/jquery.loadmask.css') }}">

    @include('dashboard::layouts.includes.styles')
	@yield('styles')
	<!--[if lte IE 9]>
		<link href="{{asset('packages/frozennode/administrator/css/browsers/lte-ie9.css')}}" media="all" type="text/css" rel="stylesheet">
	<![endif]-->
</head>
<body>
	<div id="wrapper">
		@include('administrator::partials.header')

		{!! $content !!}

		@include('administrator::partials.footer')
	</div>
	<div style="display: none;">
		{!! HTML::script('packages/frozennode/administrator/js/jquery/jquery-1.8.2.min.js') !!}
		{!! HTML::script('packages/frozennode/administrator/js/jquery/jquery-ui-1.10.3.custom.min.js') !!}
		{!! HTML::script('packages/frozennode/administrator/js/jquery/customscroll/jquery.customscroll.js') !!}
		{!! HTML::script('packages/frozennode/administrator/js/page.js') !!}
        {!! HTML::script('packages/mambo/cms/js/backbone/underscore-min.js') !!}
        {!! HTML::script('packages/mambo/cms/js/backbone/backbone-min.js') !!}
        {!! HTML::script('packages/mambo/cms/js/jquery-loadmask/jquery.loadmask.min.js') !!}
        {!! HTML::script('packages/mambo/cms/js/application.js') !!}

		@yield('scripts')
	</div>
</body>
</html>
