<!DOCTYPE html>
<html lang="<?php echo config('application.language') ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<title>{{ config('administrator.title') }}</title>
	<link media="all" type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link media="all" type="text/css" rel="stylesheet" href="{{ asset('packages/mambo/cms/css/administrator.css') }}">
    {!! HTML::style('packages/mambo/cms/js/jquery-loadmask/jquery.loadmask.css') !!}
	@yield('styles')
	<!--[if lte IE 9]>
		<link href="{{asset('packages/frozennode/administrator/css/browsers/lte-ie9.css')}}" media="all" type="text/css" rel="stylesheet">
	<![endif]-->
</head>
<body>
	<div id="wrapper">
		@yield('content')
	</div>
	<div style="display: none;">
		{!! HTML::script('packages/frozennode/administrator/js/jquery/jquery-1.8.2.min.js') !!}
		{!! HTML::script('packages/frozennode/administrator/js/jquery/jquery-ui-1.10.3.custom.min.js') !!}
		{!! HTML::script('packages/frozennode/administrator/js/jquery/customscroll/jquery.customscroll.js') !!}
		{!! HTML::script('packages/frozennode/administrator/js/page.js') !!}
        {!! HTML::script('packages/mambo/cms/js/lodash/lodash.min.js') !!}
        {!! HTML::script('packages/mambo/cms/js/jquery-loadmask/jquery.loadmask.min.js') !!}
        {!! HTML::script('packages/mambo/cms/js/application.js') !!}
	</div>
</body>
</html>
