@if( isset($css) && is_array($css) )
    @foreach( $css as $link )
        {!! HTML::style($link) !!}
    @endforeach
@endif
