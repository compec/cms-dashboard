@if( Session::has('message') )
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if( $errors->any() )
    <div class="form-error">
        <ul>
            @foreach( $errors->all() as $error )
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
