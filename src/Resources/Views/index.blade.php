<div id="admin_page" class="with_sidebar">
    <div id="content">
        <div class="table_container">
            <div class="results_header">
                <h2 data-bind="text: modelTitle">Dashboard</h2>
            </div>
        </div>
        <h1 class="welcome">
            {{ config('cms.configuration.welcome') }}
        </h1>
    </div>
</div>
