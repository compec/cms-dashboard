<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CmsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		
		$this->call('AdministratorsTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('ComponentTypesTableSeeder');
		
	}

}
