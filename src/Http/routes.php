<?php

View::composer(array('administrator::layouts.default'), function($view)
{
    $view->css += array(
        'fix' => asset('packages/mambo/cms/css/frozennode_administrator_fix.css')
    );
});

Route::group(array('prefix' => Config::get('administrator.uri'), 'middleware' => 'Frozennode\Administrator\Http\Middleware\ValidateAdmin'), function() {

    Route::get('page/{page}', array(
        'as' => 'admin_page',
        function ($page) {
            $router = app()['router']; // get router
            $route = $router->current(); // get current route
            $request = Request::instance(); // get http request
            $nameController = ucfirst(camel_case($page));
            $controller = 'Mambo\Cms\\' . $nameController . '\Http\Controllers\\' . $nameController . 'Controller'; // generate controller name
            $action = 'getIndex'; // action

            $dispatcher = new \Illuminate\Routing\ControllerDispatcher($router, app());

            return $dispatcher->dispatch($route, $request, $controller, $action);
        }
    ));
});
