<?php namespace Mambo\Cms\Dashboard\Http\Controllers;

use Mambo\Cms\Core\Http\Controllers\HttpController;
use Mambo\Cms\Core\Contracts\BackendLayout;

class DashboardController extends HttpController {

    use BackendLayout;

    protected $layout = "dashboard::layouts.default";

    protected $componentRepository;

    public function __construct()
    {
        $this->initLayout($this);
    }

    public function getIndex()
    {

        return $this->render($this, 'dashboard::index');
    }
}
