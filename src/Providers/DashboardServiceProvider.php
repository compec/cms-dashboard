<?php namespace Mambo\Cms\Dashboard\Providers;

use Frozennode\Administrator\AdministratorServiceProvider;

use Illuminate\Foundation\AliasLoader;

use View;
use File;

class DashboardServiceProvider extends AdministratorServiceProvider {

    public function register()
    {
        parent::register();

        $this->registerNamespace();

        // Register Iluminate\Html
        $this->registerIluminateHtmlPackage();
        // Register MaatwebsiteExcel
        $this->registerMaatwebsiteExcelPackage();
        // Register orchestra/imagine
        $this->registerOrchestraImaginePackage();
        // Register cviebrock/image-validator
        $this->registerCviebrockImageValidatorPackage();
        
    }

    public function boot()
    {
        parent::boot();

        $this->publishConfig();

        $this->publishRoutes();

        $this->publishAssets();

        $this->publishSeeds();
        
        $this->publishLang();
    }

    private function publishConfig()
    {
        if( !File::exists( config_path('cms/settings') ) ){
            File::makeDirectory( config_path('cms') );
            File::makeDirectory( config_path('cms/settings') );
        }

        if( !File::exists( config_path('cms') ) ){
            File::makeDirectory( config_path('cms') );
            File::makeDirectory( config_path('cms/widgets') );
        }
        
        $this->publishes([
            __DIR__ . '/../Config/cms/configuration.php' => config_path('cms/configuration.php')
        ]);
    }

    private function publishRoutes()
    {
        include(__DIR__ . '/../Http/routes.php');
    }

    private function publishAssets()
    {
        $this->publishes([
            __DIR__ . '/../../public' => public_path('packages/mambo/cms/'),
        ], 'public');
    }

    private function publishLang()
    {
        $this->publishes([
            __DIR__ . '/../Resources/Lang' => base_path('resources/lang/'),
        ]);
    }
    
    private function publishSeeds()
    {
        $this->publishes([
            __DIR__ . '/../Database/seeds' => base_path('database/seeds/cms/'),
        ]);
    }

    private function registerNamespace()
    {
        View::addNamespace('dashboard', __DIR__.'/../Resources/Views/');
    }

    private function registerIluminateHtmlPackage()
    {
        $this->app->register('Illuminate\Html\HtmlServiceProvider');

        $loader = AliasLoader::getInstance();
        $loader->alias('Form', 'Illuminate\Html\FormFacade');
        $loader->alias('HTML', 'Illuminate\Html\HtmlFacade');
    }      

    private function registerMaatwebsiteExcelPackage()
    {
        $this->app->register('Maatwebsite\Excel\ExcelServiceProvider');

        $loader = AliasLoader::getInstance();
        $loader->alias('Excel', 'Maatwebsite\Excel\Facades\Excel');
    }

    private function registerOrchestraImaginePackage()
    {
        $this->app->register('Orchestra\Imagine\ImagineServiceProvider');

        $loader = AliasLoader::getInstance();
        $loader->alias('Imagine', 'Orchestra\Imagine\Facade');
    }
    
    private function registerCviebrockImageValidatorPackage()
    {
        $this->app->register('Cviebrock\ImageValidator\ImageValidatorServiceProvider');
    }
    
}
