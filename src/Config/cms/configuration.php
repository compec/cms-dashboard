<?php

return array(
    'typ' => '<<view name of thank you page>>',
    // configuración para crear directorios necesarios de los componentes
    'folders' => array(
        'cms.pages.images.sizes',
        'cms.widgets.slide.uploads',
        'cms.widgets.gallery.uploads',
    ),
    'pagination' => array(
        'rowsPerPage' => 10
    ),
    'welcome' => '<<Welcome to CMamboS>>'
);
