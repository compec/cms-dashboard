function readURL(input, elt) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(elt).attr('src', e.target.result).show();
        };

        reader.readAsDataURL(input.files[0]);
    }
};

var normalize = (function() {
    var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
        to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
        mapping = {};

    for(var i = 0, j = from.length; i < j; i++ ) {
        mapping[ from.charAt( i ) ] = to.charAt( i );   
    }

    return function( str ) {
        var ret = [];
        for( var i = 0, j = str.length; i < j; i++ ) {
            var c = str.charAt( i );
            if( mapping.hasOwnProperty( str.charAt( i ) ) ) {
                ret.push( mapping[ c ] );   
            } else {
                ret.push( c );
            }
        }

        return ret.join( '' );
    };
 
 })();

function slug (value) {
    return value.toLowerCase().replace(/-+/g, '').replace(/\s+/g, '-').replace(/[^a-z0-9-]/g, '');
};

var $blockMessage = null;
var $blockContainer = null;
$(function(){    
    $blockContainer = $('div.block-container');
    $blockMessage = $('div.block-messages');

    $( document ).ajaxSend(function() {
        $('div.error').removeClass('error').find('em').remove();
    });

    $(document).ajaxError(function(event, xhr, settings, errorText){
        switch( xhr.status ) { // 
            case 422:
                var errors = $.parseJSON(xhr.responseText);
                var $form = $('form');

                $form.find('div.error').find('em').remove();
                $form.find('div.error').removeClass('error');

                for(var name in errors ) {
                    var error = errors[name];
                    var $e = $('[data-input-name="'+name+'"]');
                    $e.append('<em>'+error[0]+'</em>');
                    $e.addClass('error');
                }
            break;
            case 500:
                $blockContainer.unmask();
            break;
        }
    });

    $(document).ajaxComplete(function(event, xhr, settings, errorText){
        var response = $.parseJSON(xhr.responseText);
        switch( xhr.status ) { // 
            case 200:
                var html = '<div class="alert alert-success">%s</div>';
                if( response.error ) {
                    html = '<div class="alert alert-error">%s</div>';    
                }
                
                $blockMessage.html( html.replace('%s', response.message) ).fadeIn();
                setTimeout(function(){
                    $blockMessage.fadeOut();
                }, 5000);
            break;
        }

        $blockContainer.unmask();
    });    
});