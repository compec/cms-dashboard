$(function(){
	$.fn.simpletabs = function() {
		var tabs = this;
		tabs.find('ul.tabs-head a').click(function(){
			tabs.find('a.active').removeClass('active');
			var $tabLink = $(this).addClass('active');
			var panel = $tabLink.data('panel')
			tabs.find('section.tab-panel').hide();
			tabs.find('section#' + panel).show();
		});
	};

});